{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad.IO.Class ( liftIO )

import Database.PostgreSQL.Simple as PG
import qualified Database.Beam as Beam
import qualified Database.Beam.Postgres as Beam

import Lib

connInfo :: PG.ConnectInfo
connInfo = PG.ConnectInfo
  { connectPort = 5432
  , connectHost = "localhost"
  , connectDatabase = "hitmen"
  , connectUser = "postgres"
  , connectPassword = ""
  }

main :: IO ()
main = do
  conn <- PG.connect connInfo

  Beam.runBeamPostgresDebug putStrLn conn $ do
    latest <- Beam.runSelectReturningList (Beam.select latestHits)
    totalBountyAwarded1 <- Beam.runSelectReturningList (Beam.select (totalBountyAwarded (HitmanID 1)))

    Beam.runUpdate (increaseListBounty 1337 (MarkID 4))

    liftIO $ putStrLn "===== LATEST HITS ====="
    liftIO $ print latest

    liftIO $ putStrLn "===== TOTAL BOUNTY AWARDED TO HITMAN 1 ====="
    liftIO $ print totalBountyAwarded1

  PG.close conn
