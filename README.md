# haskell-orm-comparison

A bunch of reference implementations of various queries, done using
each of the following DB libraries:

* [Beam](http://hackage.haskell.org/package/beam)
* [Opaleye](http://hackage.haskell.org/package/opaleye)
* [Squeal](http://hackage.haskell.org/package/squeal-postgresql)
* [Hasql](http://hackage.haskell.org/package/hasql)
* [Selda](http://hackage.haskell.org/package/selda)

For more context, see the original blog post:
[Which type-safe database library should you use?](https://williamyaoh.com/posts/2019-12-14-typesafe-db-libraries.html)

## Results

Per `tokei src/`:

|         | Line count (actual code) |
|---------|--------------------------|
| Beam    | 287                      |
| Opaleye | 348                      |
| Squeal  | 379                      |
| Hasql   | 278                      |
| Selda   | 245                      |

## Building and running

You'll need a local Postgres server. Once it's running, create the
tables using the SQL commands in the first half of `tables.sql`.
Then create test data using the SQL commands in `seed-data.sql`.

Build an implementation of your choice using Stack. Each implementation
has an executable that will connect to the database and run some
example queries. Edit the connection information in the relevant `Main.hs`
and run.

## An invitation

If you've gotten this far, why not give a library of your choice a shot? Write a
new query to see how the library feels to use. For instance, you could try
getting all marks that required a long chase. Or, take one of the
implementations as a template and start on a project of your own. If you're
thinking of trying Opaleye, Saurabh Nanda has written some [excellent docs](http://haskell.vacationlabs.com/en/latest/docs/opaleye/opaleye.html)
to get you started.
