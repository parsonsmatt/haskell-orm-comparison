{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Data.Scientific ( Scientific )

import Control.Arrow

import Database.PostgreSQL.Simple as PG

import qualified Opaleye as Opaleye

import Lib

connInfo :: PG.ConnectInfo
connInfo = PG.ConnectInfo
  { connectPort = 5432
  , connectHost = "localhost"
  , connectDatabase = "hitmen"
  , connectUser = "william"
  , connectPassword = ""
  }

main :: IO ()
main = do
  conn <- PG.connect connInfo

  latest :: [(Hitman, Mark)] <- Opaleye.runSelect conn latestHits
  totalBountyAwarded1 :: [Scientific] <-
    Opaleye.runSelect conn (arr (const (HitmanID 1)) >>> totalBountyAwarded)

  Opaleye.runUpdate_ conn (increaseListBounty 1337 (MarkID 4))

  putStrLn "===== LATEST HITS ====="
  print latest

  putStrLn "===== TOTAL BOUNTY AWARDED TO HITMAN 1 ====="
  print totalBountyAwarded1

  PG.close conn
