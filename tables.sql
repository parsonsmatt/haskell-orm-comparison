CREATE TABLE handlers
  ( id SERIAL PRIMARY KEY NOT NULL UNIQUE
  , codename TEXT NOT NULL UNIQUE
  , created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
  , updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
  , CONSTRAINT "handlers_valid_updated_at" CHECK (updated_at >= created_at)
  );

CREATE TABLE hitmen
  ( id SERIAL PRIMARY KEY NOT NULL UNIQUE
  , codename TEXT NOT NULL UNIQUE
  , handler_id INT NOT NULL
  , created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
  , updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
  , CONSTRAINT "fk_hitmen_handlers" FOREIGN KEY (handler_id) REFERENCES handlers (id)
  , CONSTRAINT "hitmen_valid_updated_at" CHECK (updated_at >= created_at)
  );

CREATE TABLE marks
  ( id SERIAL PRIMARY KEY NOT NULL UNIQUE
  , list_bounty BIGINT NOT NULL
  , first_name TEXT NOT NULL
  , last_name TEXT NOT NULL
  , description TEXT NULL
  , created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
  , updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
  , CONSTRAINT "marks_valid_updated_at" CHECK (updated_at >= created_at)
  );

CREATE TABLE pursuing_marks
  ( hitman_id INT NOT NULL
  , mark_id INT NOT NULL
  , created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
  , updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
  , CONSTRAINT "pk_pursuing_marks" PRIMARY KEY (hitman_id, mark_id)
  , CONSTRAINT "pursuing_marks_valid_updated_at" CHECK (updated_at >= created_at)
  );

CREATE TABLE erased_marks
  ( hitman_id INT NOT NULL
  , mark_id INT NOT NULL
  , awarded_bounty BIGINT NOT NULL
  , created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
  , updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
  , CONSTRAINT "pk_erased_marks" PRIMARY KEY (hitman_id, mark_id)
  , CONSTRAINT "erased_marks_valid_updated_at" CHECK (updated_at >= created_at)
  );

--

DROP TABLE erased_marks;
DROP TABLE pursuing_marks;
DROP TABLE marks;
DROP TABLE hitmen;
DROP TABLE handlers;
